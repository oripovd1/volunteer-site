import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import VolunteerHelp from "../components/volunteer-help"
import be_volunteer from '../images/be_volunteer.png'
import opportunity from '../images/opportunity.png'
import aslkuchbirlikda from '../images/aslkuchbirlikda.png'
import { FaTelegram, FaInstagram, FaFacebook, FaLongArrowAltUp } from 'react-icons/fa'
import AnchorLink from 'react-anchor-link-smooth-scroll'
//import Donate from "../components/donate"

const IndexPage = () => (
  <Layout lang="uz">
    <SEO title="Home" />
    <section className="bg-splash" id="top">
      <div className="container">
        <div className="row mt-lg-5">
          <div className="col-md-6 text-white bg-splash-content">
            <h1>Koronavirus pandemiyasi paytida o'zaro yordam</h1>
            <h2 className="h1">#aslkuchbirlikda</h2>
          </div>
          <div className="col-md-6 mt-md-4">
            <img src={aslkuchbirlikda} alt="Asl kuch birlikda" className="splash-img" />
          </div>
        </div>
      </div>
    </section>
    <section className="volunteer" id="volunteer">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 order-2 order-lg-0 offset-md-4 offset-lg-0 mt-5 mt-md-0">
            <img src={be_volunteer} alt="Be volunteer" />
          </div>
          <div className="col-lg-6">
            <div className="volunteer-content">
              <h2>Ko'ngilli bo'ling</h2>
              <p>Mamlakatimizning ko'plab aholisi izolyatsiyada bo’lishga majbur. Ko'ngillilar oziq-ovqat va dori-darmon sotib olishda yordam berishlari, kundalik muammolarni hal qilishlari va mintaqaviy shtablarning ishini tashkil qilishlari mumkin. Birgalikda biz barcha qiyinchiliklarni yengamiz va har biringizni qo'llab-quvvatlaymiz.</p>
              <a href="https://t.me/aslkuchbirlikda" target="_blank" rel="noreferrer noopener" className="btn btn-primary btn-lg">Ko'ngilli bo'ling</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <VolunteerHelp />
    <section className="donate" id="donate">
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="donate-content">
              <h2>Men yordam berishni xohlayman</h2>
              <p>Qiyin vaziyatda hammamiz har qachongidan ham yordamga muhtojmiz. Deyarli hozirda barcha - yirik kompaniyalardan tortib mamlakatimizning oddiy aholisigacha uning barcha ko'rinishlariga yordam ko'rsatmoqdalar.</p>
              <p>Masalan, IT-kompaniyalar o'z imkoniyatlaridan kelib chiqib, maktablarda masofaviy o'qishni qo'llab-quvvatlamoqdalar, pardozlagich vositalari ishlab chiqaruvchi korxonalar antiseptik preparatlarni, tijorat tashkilotlari profilaktika tadbirlari uchun pul mablag'larini ajratmoqdalar.</p>
              <p>Agar siz tibbiy asbob-uskunalardan tortib bepul xizmat ko'rsatishgacha biron bir usulda yordam bera olsangiz - shaklni to'ldiring!</p>
              <a href="https://t.me/aslkuchbirlikda" target="_blank" rel="noreferrer noopener" className="btn btn-primary btn-lg">Men yordam berishni xohlayman</a>
            </div>
          </div>
          <div className="col-lg-6 mt-5 mt-lg-0">
            <img src={be_volunteer} alt="Хочу помочь" />
          </div>
        </div>
      </div>
    </section>
    <section className="contact">
      <div className="container">
        <div className="contact-card">
          <div className="row">
            <div className="col-12">
              <h1>Biz bilan bog'laning</h1>
              <a href="https://t.me/aslkuchbirlikda" target="_blank" rel="noreferrer noopener" className="btn btn-primary btn-telegram">
                <FaTelegram size="20px" /> <span>Telegram</span>
              </a>
              <a href="https://www.instagram.com/aslkuchbirlikda/" target="_blank" rel="noreferrer noopener" className="btn btn-primary btn-insta mx-2">
                <FaInstagram size="20px" /> <span>Instagram</span>
              </a>
              <a href="https://www.facebook.com/aslkuchbirlikda/" target="_blank" rel="noreferrer noopener" className="btn btn-primary btn-fb mt-2 mt-md-0">
                <FaFacebook size="20px" /> <span>Facebook</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section className="donate" id="opportunities">
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="donate-content">
              <h2>Belgilarini aniqlashda yordam</h2>
              <p>Mamlakatlar koronavirusni aniqlashga ko'proq e'tibor berishlari, virus bilan zararlanganlarning izolyatsiyasini ta'minlashlari va ularning kim bilan muloqotda bo'lganlgini aniqlashlari kerak. Jahon Sog`liqni Saqlash Tashkiloti barcha davlatlarni har bir shubhali holatni tekshirishga, agar taxmin tasdiqlansa, u odamni izolyatsiya qilish, shuningdek birinchi belgilariga ega bo'lganlarning hammasini aniqlashga ko’proq e'tibor berishga chaqirdi. Ushbu tadbirda yordam berishga tayyor bo'lgan ko'ngillilarni safimizga chaqiramiz!</p>
            </div>
          </div>
          <div className="col-lg-6">
            <img src={opportunity} alt="Возможности" />
          </div>
        </div>
      </div>
    </section>
    <footer className="pt-5 pb-4 text-white">
      <div className="container">
        <div className="row">
          <div className="col text-center mb-4 mb-lg-5 footer-social">
            <p className="lead font-weight-bold mb-2">Biz ijtimoiy tarmoqlarda:</p>
            <a href="https://t.me/aslkuchbirlikda" target="_blank" rel="noopener noreferrer" className="d-block d-md-inline-block text-white text-decoration-none font-weight-bold narrow-up mb-2 mb-lg-0"><FaTelegram size="22px" /> <span>@aslkuchbirlikda</span></a>
            <a href="https://www.instagram.com/aslkuchbirlikda/" target="_blank" rel="noopener noreferrer" className="d-block d-md-inline-block text-white text-decoration-none font-weight-bold mx-4 narrow-up mb-2 mb-lg-0"><FaInstagram size="22px" /> <span className="ml-1">AslKuchBirlikda</span></a>
            <a href="https://www.facebook.com/aslkuchbirlikda/" target="_blank" rel="noopener noreferrer" className="d-block d-md-inline-block text-white text-decoration-none font-weight-bold narrow-up mb-2 mb-lg-0"><FaFacebook size="22px" /> <span className="ml-1">AslKuchBirlikda</span></a>
          </div>
        </div>
      </div>
      <nav className="navbar footer-nav">
        <div className="col-12 col-lg-2 text-center text-lg-left order-3 order-lg-0 mt-3 mt-md-0">
          <a href="https://udevs.io" className="small d-inline-block mt-2" target="_blank" rel="noopener noreferrer">Created by Udevs</a>
        </div>
        <div className="footer-nav-links col-12 col-lg-8 d-md-flex justify-content-around text-center pt-4 pb-3 my-md-0">
          <AnchorLink href="/volunteer" className="d-block mb-2 mb-lg-0">Ko'ngilli bo'ling</AnchorLink>
          <AnchorLink href="/help" className="d-block mb-2 mb-lg-0">Ko'ngillilar yordami</AnchorLink>
          <AnchorLink href="/donate" className="d-block mb-2 mb-lg-0">Men yordam berishni xohlayman</AnchorLink>
          <AnchorLink href="/opportunities" className="d-block mb-2 mb-lg-0">Imkoniyatlar</AnchorLink>
        </div>
        <div className="col-12 col-lg-2 text-center text-lg-right narrow-up">
          <AnchorLink href="/top" className="small d-inline-block mt-2">Yuqoriga <FaLongArrowAltUp size="16px" /></AnchorLink>
        </div>
      </nav>
    </footer>
  </Layout >
)

export default IndexPage
