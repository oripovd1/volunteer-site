import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import VolunteerHelpRu from "../components/volunteer-help-ru"
import be_volunteer from '../images/be_volunteer.png'
import opportunity from '../images/opportunity.png'
import aslkuchbirlikda from '../images/aslkuchbirlikda.png'
import { FaTelegram, FaInstagram, FaFacebook, FaLongArrowAltUp } from 'react-icons/fa'
import AnchorLink from 'react-anchor-link-smooth-scroll'
//import Donate from "../components/donate"

const Ru = () => (
  <Layout lang="ru">
    <SEO title="Home" />
    <section className="bg-splash" id="top">
      <div className="container">
        <div className="row mt-lg-5">
          <div className="col-md-6 text-white bg-splash-content">
            <h1>Акция взаимопомощи во время пандемии коронавируса</h1>
            <h2 className="h1">#aslkuchbirlikda</h2>
          </div>
          <div className="col-md-6 mt-lg-4">
            <img src={aslkuchbirlikda} alt="Asl kuch birlikda" className="splash-img" />
          </div>
        </div>
      </div>
    </section>
    <section className="volunteer" id="volunteer">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 order-2 order-lg-0 offset-md-4 offset-lg-0 mt-5 mt-md-0">
            <img src={be_volunteer} alt="Be volunteer" />
          </div>
          <div className="col-lg-6">
            <div className="volunteer-content">
              <h2>Стать волонтёром</h2>
              <p>Многие жители нашей страны вынуждены находиться в изоляции. Волонтёры могут помочь в покупке продуктов и лекарств, решении бытовых проблем и в организации работы региональных штабов. Вместе мы справимся со всеми трудностями и поддержим каждого.</p>
              <a href="https://t.me/aslkuchbirlikda" target="_blank" rel="noreferrer noopener" className="btn btn-primary btn-lg">Стать волонтёром</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <VolunteerHelpRu />
    <section className="donate" id="donate">
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="donate-content">
              <h2>Я хочу помочь</h2>
              <p>В сложной ситуации нам всем как никогда нужна поддержка. Уже сейчас все – от крупных компаний до простых жителей наших страны оказывают помощь во всех ее проявлениях.</p>
              <p>Например, IT-компании за счет собственных мощностей обеспечивают поддержку дистанционного обучения в школах, парфюмерные компании безвозмездно передают антисептические средства, коммерческие организации жертвуют деньги для профилактических мер.</p>
              <p>Если вы можете помочь любым способом – от предоставления медицинского инвентаря до оказания безвозмездных услуг – заполните форму.</p>
              <a href="https://t.me/aslkuchbirlikda" target="_blank" rel="noreferrer noopener" className="btn btn-primary btn-lg">Я хочу помочь</a>
            </div>
          </div>
          <div className="col-lg-6 mt-5 mt-lg-0">
            <img src={be_volunteer} alt="Хочу помочь" />
          </div>
        </div>
      </div>
    </section>
    <section className="contact">
      <div className="container">
        <div className="contact-card">
          <div className="row">
            <div className="col-12">
              <h1>Свяжитесь с нами</h1>
              <a href="https://t.me/aslkuchbirlikda" target="_blank" rel="noreferrer noopener" className="btn btn-primary btn-telegram">
                <FaTelegram size="20px" /> <span>Telegram</span>
              </a>
              <a href="https://www.instagram.com/aslkuchbirlikda/" target="_blank" rel="noreferrer noopener" className="btn btn-primary btn-insta mx-2">
                <FaInstagram size="20px" /> <span>Instagram</span>
              </a>
              <a href="https://www.facebook.com/aslkuchbirlikda/" target="_blank" rel="noreferrer noopener" className="btn btn-primary btn-fb mt-2 mt-md-0">
                <FaFacebook size="20px" /> <span>Facebook</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section className="donate" id="opportunities">
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="donate-content">
              <h2>Помощь в определение симптомов</h2>
              <p>Страны должны уделять больше внимания тестированию на коронавирус, обеспечивать изоляцию заразившихся людей и выявлять их контакты. ВОЗ настоятельно рекомендовал всем странам проверять каждый вызывающий подозрение случай, если предположение подтвердится, то изолировать человека, а также выявить всех, у кого появятся первые симптомы. Мы призываем волонтеров, готовых помочь нам в этом деле!</p>
            </div>
          </div>
          <div className="col-lg-6">
            <img src={opportunity} alt="Возможности" />
          </div>
        </div>
      </div>
    </section>
    <footer className="pt-5 pb-4 text-white">
      <div className="container">
        <div className="row">
          <div className="col text-center mb-4 mb-lg-5 footer-social">
            <p className="lead font-weight-bold mb-2">Мы в социальных сетях:</p>
            <a href="https://t.me/aslkuchbirlikda" target="_blank" rel="noopener noreferrer" className="d-block d-md-inline-block text-white text-decoration-none font-weight-bold narrow-up mb-2 mb-lg-0"><FaTelegram size="22px" /> <span>@aslkuchbirlikda</span></a>
            <a href="https://www.instagram.com/aslkuchbirlikda/" target="_blank" rel="noopener noreferrer" className="d-block d-md-inline-block text-white text-decoration-none font-weight-bold mx-4 narrow-up mb-2 mb-lg-0"><FaInstagram size="22px" /> <span className="ml-1">AslKuchBirlikda</span></a>
            <a href="https://www.facebook.com/aslkuchbirlikda/" target="_blank" rel="noopener noreferrer" className="d-block d-md-inline-block text-white text-decoration-none font-weight-bold narrow-up mb-2 mb-lg-0"><FaFacebook size="22px" /> <span className="ml-1">AslKuchBirlikda</span></a>
          </div>
        </div>
      </div>
      <nav className="navbar footer-nav">
        <div className="col-12 col-lg-2 text-center text-lg-left order-3 order-lg-0 mt-3 mt-md-0">
          <a href="https://udevs.io" className="small d-inline-block mt-2" target="_blank" rel="noopener noreferrer">Created by Udevs</a>
        </div>
        <div className="footer-nav-links col-12 col-lg-8 d-md-flex justify-content-around text-center pt-4 pb-3 my-md-0">
          <AnchorLink href="/volunteer" className="d-block mb-2 mb-lg-0">Стать волонтёром</AnchorLink>
          <AnchorLink href="/help" className="d-block mb-2 mb-lg-0">Волонтёрская помощь</AnchorLink>
          <AnchorLink href="/donate" className="d-block mb-2 mb-lg-0">Я хочу помочь</AnchorLink>
          <AnchorLink href="/opportunities" className="d-block mb-2 mb-lg-0">Возможности</AnchorLink>
        </div>
        <div className="col-12 col-lg-2 text-center text-lg-right narrow-up">
          <AnchorLink href="/top" className="small d-inline-block mt-2">Наверх <FaLongArrowAltUp size="16px" /></AnchorLink>
        </div>
      </nav>
    </footer>
  </Layout >
)

export default Ru
