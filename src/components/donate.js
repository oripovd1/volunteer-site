import React from 'react';
import be_volunteer from '../images/be_volunteer.png'
//import { FaTimes } from 'react-icons/fa'

export default class Donate extends React.Component {
    state = { isPopup: false }
    handlePopup = () => {
        this.setState({ isPopup: !this.state.isPopup })
    }
    render() {
        //const { isPopup } = this.state;
        return (
            <>
                <section className="donate" id="donate">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6">
                                <div className="donate-content">
                                    <h2>Men yordam berishni xohlayman</h2>
                                    <p>Qiyin vaziyatda hammamiz har qachongidan ham yordamga muhtojmiz. Deyarli hozirda barcha - yirik kompaniyalardan tortib mamlakatimizning oddiy aholisigacha uning barcha ko'rinishlariga yordam ko'rsatmoqdalar.</p>
                                    <p>Masalan, IT-kompaniyalar o'z imkoniyatlaridan kelib chiqib, maktablarda masofaviy o'qishni qo'llab-quvvatlamoqdalar, pardozlagich vositalari ishlab chiqaruvchi korxonalar antiseptik preparatlarni, tijorat tashkilotlari profilaktika tadbirlari uchun pul mablag'larini ajratmoqdalar.</p>
                                    <p>Agar siz tibbiy asbob-uskunalardan tortib bepul xizmat ko'rsatishgacha biron bir usulda yordam bera olsangiz - shaklni to'ldiring!</p>
                                    <a href="https://t.me/aslkuchbirlikdabot" target="_blank" rel="noreferrer noopener" className="btn btn-primary btn-lg">Men yordam berishni xohlayman</a>
                                </div>
                            </div>
                            <div className="col-lg-6 mt-5 mt-lg-0">
                                <img src={be_volunteer} alt="Хочу помочь" />
                            </div>
                        </div>
                    </div>
                </section>
                {/* <section className={`popup ${isPopup ? 'show' : ''}`}>
                    <button onClick={this.handlePopup} className="popup-close">
                        <FaTimes size="22px" fill="#ffffff" />
                    </button>
                    <div className="popup-content">
                        <div className="popup-container">
                            <h1 className="text-center">Хочу помочь</h1>
                            <form action="post">
                                <div className="form-group">
                                    <label htmlFor="entity">Физическое или юридическое лицо</label>
                                    <select name="entity" id="entity" className="form-control">
                                        <option>Выберите лицо</option>
                                        <option>Я физическое лицо</option>
                                        <option>Я представляю юридическое лицо</option>
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="full_name">ФИО</label>
                                    <input type="text" name="full_name" id="full_name" placeholder="Введите ваше ФИО" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="phone">Контактный номер телефона</label>
                                    <input type="tel" name="phone_number" id="phone" placeholder="+998971234567" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="body">Опишите подробно, какую помощь вы можете предложить</label>
                                    <textarea name="body" id="body" cols="30" rows="10" placeholder="Введите описание" className="form-control"></textarea>
                                </div>
                                <button className="btn btn-primary btn-block">Отправить</button>
                            </form>
                        </div>
                    </div>
                </section> */}
            </>
        )
    }
}