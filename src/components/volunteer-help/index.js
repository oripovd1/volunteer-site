import React from 'react';
import Loadable from 'react-loadable';

const VolunteerHelp = Loadable({
    loader: () => import('./volunteer-help'),
    loading() {
        return <div>Loading...</div>
    } 
})

export default () => <VolunteerHelp />