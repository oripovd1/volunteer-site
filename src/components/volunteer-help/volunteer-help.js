import React from 'react';
import Carousel from 'nuka-carousel';
import help2 from '../../images/help2.png';
import help3 from '../../images/help3.png';
import deliver from '../../images/deliver-help.png';
import online from '../../images/online-help.png';
import { FaChevronRight, FaChevronLeft } from 'react-icons/fa'

const isClient = typeof window !== 'undefined';

class VolunteerHelp extends React.Component {
    state = {
        viewportWidth: 0
    }
    componentDidMount() {
        if (isClient) {
            window.addEventListener('resize', this.updateWindowDimensions);
            setTimeout(() => {
                this.updateWindowDimensions();
            }, 500);
        }
    };
    componentWillUnmount() {
        if (isClient) window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions = () => {
        this.setState({ viewportWidth: window.innerWidth });
    }

    render() {
        const { viewportWidth } = this.state;
        const isMobile = Boolean(viewportWidth <= 767);
        const { ru } = this.props;
        return (
            <section className="help" id="help">
                <div className="container">
                    <h1 className="text-center mb-5">{ru ? 'Волонтёрская помощь' : 'Ko\'ngillilar yordami'}</h1>
                    <div className="row">
                        <Carousel autoplay slidesToShow={isMobile ? 1 : 2} slidesToScroll={1} wrapAround renderBottomCenterControls={null}
                            renderCenterLeftControls={({ previousSlide }) => (
                                <button className="btn btn-primary btn-carousel" onClick={previousSlide}><FaChevronLeft /></button>
                            )}
                            renderCenterRightControls={({ nextSlide }) => (
                                <button className="btn btn-primary btn-carousel" onClick={nextSlide}><FaChevronRight /></button>
                            )}>
                            <div className="col">
                                <div className="card text-center">
                                    <img src={deliver} alt="Покупка и доставка товаров первой необходимости" className="card-img-top" />
                                    <div className="card-body">
                                        <h3 className="card-title">{ru ? 'Покупка и доставка товаров первой необходимости' : 'Ehtiyoj uchun eng zarur mahsulotlarni sotib olish va yetkazib berish'}</h3>
                                        <p className="card-text">{ru ? 'Людям, которые сейчас находятся дома в самоизоляции в связи с коронавирусом требуется помощь в покупке и доставке продуктов и хозяйственных товаров. Если у тебя есть автомобиль ты сможешь помочь с доставкой в труднодоступные районы.' : 'Hozirda koronavirus tufayli undan uyda himoyalanayotgan odamlar oziq-ovqat, dori-darmon va xo’jalik buyumlarini sotib olish va yetkazib berishda yordamga muhtoj. Agar sizning mashinangiz bo\'lsa, olis hududlarga yetkazib berishda yordam berishingiz mumkin.'}</p>
                                        <h5>{ru ? 'Помоги людям в условиях самоизоляции - стань волонтером!' : 'Izolyatsiya sharoitida qolgan odamlarga yordam bering - ko\'ngilli bo\'ling!'}</h5>
                                    </div>
                                </div>
                            </div>
                            <div className="col">
                                <div className="card text-center">
                                    <img src={online} alt="Онлайн-помощь" className="card-img-top" />
                                    <div className="card-body">
                                        <h3 className="card-title">{ru ? 'Онлайн-помощь' : 'Onlayn-yordam'}</h3>
                                        <p className="card-text">{ru ? 'Поскольку пожилые люди - самая уязвимая категория населения для коронавируса, в домах престарелых вводится карантин. Пожилые люди оказываются в ситуации полной изоляции и чувствуют себя одинокими. Каждый может помочь им из своего дома – просто пообщаться с ними по видеозвонку. Волонтеры могут дистанционно курировать дома престарелых и общаться с их жителями по скайпу/видеозвонкам.' : 'Keksa yoshdagilar koronavirusni yuqtirib olishga eng ko’p moyilligi bo\'lganligi sababli, qariyalar uylarida karantin joriy etiladi. Keksa yoshdagilar to’liq himoyalanish holatiga olinganida, ular o’zlarini yolg\'iz his qilar ekan. Har birimiz ularga uydan turib yordam bera olamiz - shunchaki video qo\'ng\'iroq orqali ular bilan suhbatlashing. Ko\'ngillilar qariyalar uylarini masofadan turib nazorat qilishlari va birga yashaydiganlari bilan Skype/Video qo\'ng\'iroqlar orqali aloqa qilishlari mumkin.'}</p>
                                        <h5>{ru ? 'Помоги пожилым людям спастись от одиночества - стань волонтером!' : 'Qariyalarni yolg\'izlikda qolmasligiga yordam bering - ko\'ngilli bo\'ling!'}</h5>
                                    </div>
                                </div>
                            </div>
                            <div className="col">
                                <div className="card text-center">
                                    <img src={help2} alt="Оказание психологической помощи" className="card-img-top" />
                                    <div className="card-body">
                                        <h3 className="card-title">{ru ? 'Оказание психологической помощи' : 'Ruhiy yordam ko\'rsatish'}</h3>
                                        <p className="card-text">{ru ? `В связи с широким распространением различной информации о коронавирусной инфекции люди не всегда могут воспринимать ее адекватно, возникают стрессовые реакции. Волонтеры, имеющие соотвествующую подготовку, смогут оказать психологическую поддержку людям по телефону или с использованием телемедицинских технологий. Чтобы стать волонтером по этому направлению необходимо наличие психологического образования.` : `Koronavirus infeksiyasi haqida turli xil ma'lumotlarning keng tarqalishi tufayli odamlar har doim ham uni yetarli darajada idrok eta olmaydilar, bu esa stressli o’zgarishlarga olib keladi. Tegishli tayyorgarlikka ega bo'lgan ko'ngillilar telefon orqali yoki tibbiyotga oid texnologiyalardan foydalangan holda odamlarga ruhiy yordam ko'rsatishlari mumkin. Ushbu sohada ko'ngilli bo'lish uchun siz ruhshunoslik sohasida ma'lumotga ega bo’lishingiz kerak.`}</p>
                                        <h5>{ru ? `Если ты психолог, стань волонтером и помоги людям справиться со стрессом!` : `Agar siz ruhshunos bo'lsangiz, ko'ngilli bo'ling! Odamlarga stress bilan kurashishda yordam bering!`}</h5>
                                    </div>
                                </div>
                            </div>
                            <div className="col">
                                <div className="card text-center">
                                    <img src={help3} alt="Оказание юридической поддержки" className="card-img-top" />
                                    <div className="card-body">
                                        <h3 className="card-title">{ru ? `Оказание юридической поддержки` : `Huquqiy yordam ko'rsatish`}</h3>
                                        <p className="card-text">{ru ? `В экстренной ситуации у обычный людей и малого бизнеса возникают и юридические вопросы. Волонтеры будут проводить бесплатные юридические консультации и помогать сложные ситуации, связанные с коронавирусной инфекцией.` : `Shoshilinch holatlarda oddiy odamlar va kichik biznes vakillari yuridik muammolarga duch kelishadi. Ko'ngillilar koronavirus infeksiyasi bilan bog'liq qiyin vaziyatlarda bepul yuridik maslahat va yordam ko'rsatadilar.`}</p>
                                        <h5>{ru ? `Если ты юрист, стань волонтером, и помоги людям решить возникающие проблемы!` : `Agar siz huquqshunos bo'lsangiz, ko'ngilli bo'ling! Odamlarga muammolarni hal qilishda yordam bering!`}</h5>
                                    </div>
                                </div>
                            </div>
                        </Carousel>
                    </div>
                </div>
            </section>
        )
    }
}

export default VolunteerHelp;