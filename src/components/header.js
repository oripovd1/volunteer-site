import PropTypes from "prop-types"
import React from "react"
import { Link } from 'gatsby'
import logo from '../images/logo.png'
import { FaTimes } from 'react-icons/fa'
import AnchorLink from 'react-anchor-link-smooth-scroll'
import russian from '../images/russian.png'
import uzbek from '../images/uzbek.png'

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isOpen: false }
  }
  toggle = () => {
    this.setState({ isOpen: !this.state.isOpen })
  }
  render() {
    const { siteTitle, ru } = this.props;
    const { isOpen } = this.state;
    return (
      <header>
        <nav className="navbar navbar-expand-lg navbar-light">
          <a href="/" className="navbar-brand">
            <img src={logo} alt={siteTitle} className="d-inline-block align-top" style={{ width: '40px', height: '40px' }} />
          </a>
          <button className="navbar-toggler" onClick={this.toggle} type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
            {isOpen ? <FaTimes size="30px" /> : <span className="navbar-toggler-icon"></span>}
          </button>
          <div className={`collapse navbar-collapse justify-content-between ${isOpen ? 'show' : ''}`} id="menu">
            <ul className="navbar-nav ml-0 ml-lg-4">
              <li className="nav-item"><AnchorLink href="/volunteer" className="nav-link text-center" onClick={this.toggle}>{ru ? `Стать волонтёром` : `Ko'ngilli bo'ling`}</AnchorLink></li>
              <li className="nav-item"><AnchorLink href="/help" className="nav-link mx-3 text-center" onClick={this.toggle}>{ru ? `Волонтёрская помощь` : `Ko'ngillilar yordami`}</AnchorLink></li>
              <li className="nav-item"><AnchorLink href="/donate" className="nav-link mr-3 text-center" onClick={this.toggle}>{ru ? `Хочу помочь` : `Men yordam berishni xohlayman`}</AnchorLink></li>
              <li className="nav-item"><AnchorLink href="/opportunities" className="nav-link text-center" onClick={this.toggle}>{ru ? `Возможности` : `Imkoniyatlar`}</AnchorLink></li>
            </ul>
            <Link to={ru ? '/' : '/ru'} className="btn btn-link btn-lang">
              <span>{ru ? `O'zbek` : `Русский`}</span>
              <img src={ru ? uzbek : russian} alt={ru ? `O'zbek` : `Русский`} />
            </Link>
          </div>
        </nav>
      </header>
    )
  }
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
