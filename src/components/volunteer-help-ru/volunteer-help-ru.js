import React from 'react';
import Carousel from 'nuka-carousel';
import help2 from '../../images/help2.png';
import help3 from '../../images/help3.png';
import deliver from '../../images/deliver-help.png';
import online from '../../images/online-help.png';
import { FaChevronRight, FaChevronLeft } from 'react-icons/fa'

const isClient = typeof window !== 'undefined';

class VolunteerHelpRu extends React.Component {
    state = {
        viewportWidth: 0
    }
    componentDidMount() {
        if (isClient) {
            window.addEventListener('resize', this.updateWindowDimensions);
            setTimeout(() => {
                this.updateWindowDimensions();
            }, 500);
        }
    };
    componentWillUnmount() {
        if (isClient) window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions = () => {
        this.setState({ viewportWidth: window.innerWidth });
    }

    render() {
        const { viewportWidth } = this.state;
        const isMobile = Boolean(viewportWidth <= 767);
        return (
            <section className="help" id="help">
                <div className="container">
                    <h1 className="text-center mb-5">Волонтёрская помощь</h1>
                    <div className="row">
                        <Carousel autoplay slidesToShow={isMobile ? 1 : 2} slidesToScroll={1} wrapAround renderBottomCenterControls={null}
                            renderCenterLeftControls={({ previousSlide }) => (
                                <button className="btn btn-primary btn-carousel" onClick={previousSlide}><FaChevronLeft /></button>
                            )}
                            renderCenterRightControls={({ nextSlide }) => (
                                <button className="btn btn-primary btn-carousel" onClick={nextSlide}><FaChevronRight /></button>
                            )}>
                            <div className="col">
                                <div className="card text-center">
                                    <img src={deliver} alt="Покупка и доставка товаров первой необходимости" className="card-img-top" />
                                    <div className="card-body">
                                        <h3 className="card-title">Покупка и доставка товаров первой необходимости</h3>
                                        <p className="card-text">Людям, которые сейчас находятся дома в самоизоляции в связи с коронавирусом требуется помощь в покупке и доставке продуктов и хозяйственных товаров. Если у тебя есть автомобиль ты сможешь помочь с доставкой в труднодоступные районы.</p>
                                        <h5>Помоги людям в условиях самоизоляции - стань волонтером!</h5>
                                    </div>
                                </div>
                            </div>
                            <div className="col">
                                <div className="card text-center">
                                    <img src={online} alt="Онлайн-помощь" className="card-img-top" />
                                    <div className="card-body">
                                        <h3 className="card-title">Онлайн-помощь</h3>
                                        <p className="card-text">Поскольку пожилые люди - самая уязвимая категория населения для коронавируса, в домах престарелых вводится карантин. Пожилые люди оказываются в ситуации полной изоляции и чувствуют себя одинокими. Каждый может помочь им из своего дома – просто пообщаться с ними по видеозвонку. Волонтеры могут дистанционно курировать дома престарелых и общаться с их жителями по скайпу/видеозвонкам.</p>
                                        <h5>Помоги пожилым людям спастись от одиночества - стань волонтером!</h5>
                                    </div>
                                </div>
                            </div>
                            <div className="col">
                                <div className="card text-center">
                                    <img src={help2} alt="Оказание психологической помощи" className="card-img-top" />
                                    <div className="card-body">
                                        <h3 className="card-title">Оказание психологической помощи</h3>
                                        <p className="card-text">В связи с широким распространением различной информации о коронавирусной инфекции люди не всегда могут воспринимать ее адекватно, возникают стрессовые реакции. Волонтеры, имеющие соотвествующую подготовку, смогут оказать психологическую поддержку людям по телефону или с использованием телемедицинских технологий. Чтобы стать волонтером по этому направлению необходимо наличие психологического образования.</p>
                                        <h5>Если ты психолог, стань волонтером и помоги людям справиться со стрессом!</h5>
                                    </div>
                                </div>
                            </div>
                            <div className="col">
                                <div className="card text-center">
                                    <img src={help3} alt="Оказание юридической поддержки" className="card-img-top" />
                                    <div className="card-body">
                                        <h3 className="card-title">Оказание юридической поддержки</h3>
                                        <p className="card-text">В экстренной ситуации у обычный людей и малого бизнеса возникают и юридические вопросы. Волонтеры будут проводить бесплатные юридические консультации и помогать сложные ситуации, связанные с коронавирусной инфекцией.</p>
                                        <h5>Если ты юрист, стань волонтером, и помоги людям решить возникающие проблемы!</h5>
                                    </div>
                                </div>
                            </div>
                        </Carousel>
                    </div>
                </div>
            </section>
        )
    }
}

export default VolunteerHelpRu;