import React from 'react';
import Loadable from 'react-loadable';

const VolunteerHelpRu = Loadable({
    loader: () => import('./volunteer-help-ru'),
    loading() {
        return <div>Loading...</div>
    } 
})

export default () => <VolunteerHelpRu />