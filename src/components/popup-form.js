import React from 'react';
import { FaTimes } from 'react-icons/fa'

export default class PopupForm extends React.Component {
    render() {
        return (
            <section className="popup">
                <div className="popup-close">
                    <FaTimes size="22px" fill="#ffffff" />
                </div>
                <div className="popup-content">
                    <div className="popup-container">
                        <h1 className="text-center">Хочу помочь</h1>
                        <form action="post">
                            <div className="form-group">
                                <label htmlFor="entity">Физическое или юридическое лицо</label>
                                <select name="entity" id="entity" className="form-control">
                                    <option>Выберите лицо</option>
                                    <option>Я физическое лицо</option>
                                    <option>Я представляю юридическое лицо</option>
                                </select>
                            </div>
                            <div className="form-group">
                                <label htmlFor="full_name">ФИО</label>
                                <input type="text" name="full_name" id="full_name" placeholder="Введите ваше ФИО" className="form-control" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="phone">Контактный номер телефона</label>
                                <input type="tel" name="phone_number" id="phone" placeholder="+998971234567" className="form-control" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="body">Опишите подробно, какую помощь вы можете предложить</label>
                                <textarea name="body" id="body" cols="30" rows="10" placeholder="Введите описание" className="form-control"></textarea>
                            </div>
                            <button className="btn btn-primary btn-block">Отправить</button>
                        </form>
                    </div>
                </div>
            </section>
        )
    }
}